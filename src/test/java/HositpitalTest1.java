/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.pengxing.crudmethods.databean.Inpatient;
import com.pengxing.crudmethods.databean.Medication;
import com.pengxing.crudmethods.databean.Patient;
import com.pengxing.crudmethods.databean.Surgical;
import com.pengxing.crudmethods.persistence.HospitalDAO;
import com.pengxing.crudmethods.persistence.HospitalDAOImp;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author User
 */
public class HositpitalTest1 {
    
    // Database information
    
    private final String url = "jdbc:mysql://localhost:3306/HOSPITALDB";
    private final String user = "pengxing";
    private final String password = "concordia";
    HospitalDAO hospitalDAO;
    
    /**
     * 
     * Test Create Part
     * 
     **/
    
    // Test Patient create method
   @Test
   public void testCreatePatient() throws SQLException {
       HospitalDAO hospitalDAO = new HospitalDAOImp();
        Patient patient = new Patient("Tim", "Hotan", "Cold", Timestamp.valueOf("2014-01-23 9:02:00"), Timestamp.valueOf("2014-01-31 13:00:00"));
        hospitalDAO.createPatient(patient);

//        Inpatient inpatient = new Inpatient(patient.getPATIENTID(), Timestamp.valueOf("2014-01-23 9:02:00"), "123", 1D, 1D, 1D);
//        hospitalDAO.createInpatient(inpatient);
//
//        Surgical surgical = new Surgical(patient.getPATIENTID(), Timestamp.valueOf("2014-01-23 9:02:00"), "surgery", 1D, 1D, 1D);
//        hospitalDAO.createSurgical(surgical);
//
//        Medication medication = new Medication(patient.getPATIENTID(), Timestamp.valueOf("2014-01-23 9:02:00"), "med", 1D, 1D);
//        hospitalDAO.createMedication(medication);
//
        Patient patient2 = hospitalDAO.findPatientById(6);
        assertEquals("A record was created", patient2.getPATIENTID(), 6);
   }
   
   // test inpatient record create method
    @Test
   public void testCreateInpatient() throws SQLException {
       hospitalDAO = new HospitalDAOImp();
       Inpatient inpatient = new Inpatient(5, Timestamp.valueOf("2013-04-25 9:00:00"),"X1",210.00,35.24,22.95);
       hospitalDAO.createInpatient(inpatient);
       Inpatient inpatient2 = new Inpatient();
       inpatient2 = hospitalDAO.findInpatientByID(21);
       assertEquals("A record was created", "X1", inpatient2.getROOMNUMBER());
   }
   
   // test surgical record create method
   
     @Test
      public void testCreateSurgical() throws SQLException {
       hospitalDAO = new HospitalDAOImp();
       Surgical surgical = new Surgical(5, Timestamp.valueOf("2015-01-24 11:00:00"),"Lung cancer", 2520.12, 4210.00, 914.23);
       hospitalDAO.createSurgical(surgical);
       Surgical surgical2 = new Surgical();
       surgical2 = hospitalDAO.findSurgicalByID(6);
       assertEquals("A record was created", "Lung cancer", surgical2.getSURGERY());
       
   }
      
      // test medication record create method
       @Test
       public void testCreateMedication() throws SQLException {
       hospitalDAO = new HospitalDAOImp();
       Medication medication = new Medication(5, Timestamp.valueOf("2015-01-24 11:00:00"), "Snicke", 1.35, 6.0);
       hospitalDAO.createMedication(medication);
       Medication medication2  = new Medication();
       medication2 = hospitalDAO.findMedicationByID(6);
       int t = medication2.getID();
       assertEquals("A record was created", "Snicke", medication2.getMED());
       
   }
       
       /**
        * Test Update method Part
        * 
        * @throws SQLException 
        */
       
       
      // Test patient update method
      @Test
      public void testUpdatePatient() throws SQLException {
         HospitalDAO hospitalDAO = new HospitalDAOImp();
         Patient patient = new Patient("Waine","Bruce2","Asthma2",Timestamp.valueOf("2014-01-21 9:00:00"),Timestamp.valueOf("2014-01-25 13:00:00"));
         patient.setPATIENTID(1);
         hospitalDAO.updatePatient(patient);
         Patient patient2 = new Patient();
         patient2 = hospitalDAO.findPatientById(1);
         assertNotSame("testUpdatePatient():", "Bruce2", patient2.getFIRSTNAME());
      }
      
      // Test inpatient update method
      
      @Test
      public void testUpdateInpatient() throws SQLException {
        
         HospitalDAOImp hospitalDAO = new HospitalDAOImp();
         Inpatient inpatient = new Inpatient(1,Timestamp.valueOf("2014-01-23 10:00:00"),"X2",250.00,75.24,12.95);
         inpatient.setID(1);
         hospitalDAO.updateInpatient(inpatient);
         
         
         Inpatient inpatient2 = new Inpatient();
         inpatient2 = hospitalDAO.findInpatientByID(1);
         
         assertNotSame("testUpdateInpatient():", "X2", inpatient2.getROOMNUMBER());
      }
      
      // Test medication update method
      
        @Test
      public void testUpdateMedication() throws SQLException {
         
         HospitalDAOImp hospitalDAO = new HospitalDAOImp();
         Medication medication = new Medication(1, Timestamp.valueOf("2014-01-24 12:00:00"), "Lung Replace", 1.25, 5.0);
         medication.setID(1);
         hospitalDAO.updateMedication(medication);
         
         Medication medication2 = new Medication();
         medication2 = hospitalDAO.findMedicationByID(1);
         assertNotSame("testUpdateMedication():", "Lung Replace", medication2.getMED());
      }
      
       // Tesst surgical update method
      
       @Test
      public void testUpdateSurgical() throws SQLException {
         
         HospitalDAOImp hospitalDAO = new HospitalDAOImp();
         Surgical surgical = new Surgical(1, Timestamp.valueOf("2014-01-24 12:00:00"), "Snickers", 1.25, 5.0,7.2);
         surgical.setID(1);
         hospitalDAO.updateSurgical(surgical);
         
         Surgical surgical2= new Surgical();
         surgical2.setID(1);
         surgical2 = hospitalDAO.findSurgicalByID(1);
         assertNotSame("testUpdatePatient():", "Snickers", surgical2.getSURGERY());
      }
      
      /**
       * Test Delete Part
       * @throws SQLException 
       */
      
      // Test patient record delete method
      
      
      @Test
      public void testDeletePatient() throws SQLException {
      HospitalDAOImp hospitalDAO = new HospitalDAOImp();
      Patient patient = new Patient("Wayne","Bruce","Asthma",Timestamp.valueOf("2014-01-23 9:00:00"),Timestamp.valueOf("2014-01-25 13:00:00"));
      patient.setPATIENTID(1);
      hospitalDAO.deletePatient(patient);
      if(hospitalDAO.findPatientById(1).equals(patient)){System.out.println("The record was not deleted");}else{System.out.println("the record was deleted");}
      int records = hospitalDAO.findAllPatient().size();
      assertEquals("One record is deleted:", 4, records);
      }
      
      
      
      // Test inpatient record delete method
       @Test
      public void testDeleteInpatient() throws SQLException {
      HospitalDAOImp hospitalDAO = new HospitalDAOImp();
      Inpatient inpatient = new Inpatient(1, Timestamp.valueOf("2014-01-23 9:00:00"),"A1",250.00,75.24,12.95);
      inpatient.setID(1);
      hospitalDAO.deleteInpatient(inpatient);
      int records = hospitalDAO.findAllInpatient().size();
      assertEquals("One record is deleted:", 19, records);
      }
      
      // Test surgical record delete method
      @Test
      public void testDeleteSurgical() throws SQLException {
      HospitalDAOImp hospitalDAO = new HospitalDAOImp();
      Surgical surgical = new Surgical(1, Timestamp.valueOf("2014-01-24 11:00:00"),"Lung Transplant", 2500.12, 4200.00, 934.23);
      surgical.setID(5);
      hospitalDAO.deleteSurgical(surgical);
      int records = hospitalDAO.findAllSurgical().size();
      assertEquals("One record is deleted:", 4, records);
      }
      
      // Test medication record delete method
       @Test
      public void testDeleteMedication() throws SQLException {
      HospitalDAOImp hospitalDAO = new HospitalDAOImp();
      Medication medication = new Medication(1, Timestamp.valueOf("2014-01-24 11:00:00"), "Snickers", 1.25, 5.0);
      medication.setID(5);
      hospitalDAO.deleteMedication(medication);
      int records = hospitalDAO.findAllMedication().size();
      assertEquals("One record is deleted:", 4, records);
      }
      
      /**
       * Test Find Method Part
       * @throws SQLException 
       */
    
    // Test findAllPatient() method , there are 5 records
    @Test
    public void testFindAllPatient() throws SQLException {
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        List<Patient> patientdata = hospitalDAO.findAllPatient(); 
        assertEquals("# of patients", 5, patientdata.size());
    }
 
    // Test find patient by id method 
     @Test
    public void testFindPatientById() throws SQLException {
      HospitalDAO hospitalDAO = new HospitalDAOImp();
      
      Patient patient2 = new Patient();
      patient2 = hospitalDAO.findPatientById(1);
      assertEquals("testFindPatient():", 1, patient2.getPATIENTID());
    }
    
    
    // Test findLastName(String name) method 
    @Test
     public void testFindLastName() throws SQLException {
        HospitalDAO hospitalDAO = new HospitalDAOImp();
        //Patient patient = new Patient("Wayne","Bruce","Asthma",Timestamp.valueOf("2014-01-23 9:00:00"),Timestamp.valueOf("2014-01-25 13:00:00"));
        Patient patient2 = hospitalDAO.findLastName("Wayne");
        assertEquals("testFindPatient():", "Wayne", patient2.getLASTNAME());
     }

    
      
      @Before
    public void seedDatabase() {
        final String seedDataScript = loadAsString("CreateHospitalTables.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            for (String statement : splitStatements(new StringReader(
                    seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }
    
     /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread()
                .getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream);) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }
    
    private List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<String>();
        try {
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
    
}

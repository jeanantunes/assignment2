/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pengxing.crudmethods.databean;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 *
 * @author User
 */
public class Medication {
    private int ID;
    private int PATIENTID;
    private Timestamp DATEOFMED;
    private String MED;
    private double UNITCOST;
    private double UNITS;
    
    public Medication() {
        this(-1, new Timestamp(Instant.now().toEpochMilli()), "",0.0,0.0);
    }

    public Medication(int patientid, Timestamp dateofmed, String med, double unitcost, double units) {
        super();
        this.PATIENTID = patientid;
        this.DATEOFMED = dateofmed;
        this.MED = med;
        this.UNITCOST = unitcost;
        this.UNITS = units;
       
    }
    
    
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getPATIENTID() {
        return PATIENTID;
    }

    public void setPATIENTID(int PATIENTID) {
        this.PATIENTID = PATIENTID;
    }

    public Timestamp getDATEOFMED() {
        return DATEOFMED;
    }

    public void setDATEOFMED(Timestamp DATEOFMED) {
        this.DATEOFMED = DATEOFMED;
    }

    public String getMED() {
        return MED;
    }

    public void setMED(String MED) {
        this.MED = MED;
    }

    public double getUNITCOST() {
        return UNITCOST;
    }

    public void setUNITCOST(double UNITCOST) {
        this.UNITCOST = UNITCOST;
    }

    public double getUNITS() {
        return UNITS;
    }

    public void setUNITS(double UNITS) {
        this.UNITS = UNITS;
    }

    @Override
    public String toString() {
        return "Medication{" + "ID=" + ID + ", PATIENTID=" + PATIENTID + ", DATEOFMED=" + DATEOFMED + ", MED=" + MED + ", UNITCOST=" + UNITCOST + ", UNITS=" + UNITS + '}';
    }
    
   
}

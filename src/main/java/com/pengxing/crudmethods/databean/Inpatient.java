/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pengxing.crudmethods.databean;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;


/**
 *
 * @author User
 */
public class Inpatient {
  private int ID;
  private int PATIENTID;
  private Timestamp DATEOFSTAY;
   private String ROOMNUMBER;
  private double DAILYRATE;
  private double SUPPLIES;
  private double SERVICES ;
   // Here is the join
  private List<Surgical> listOfSurgical;
  
   public Inpatient() {
        this(-1, new Timestamp(Instant.now().toEpochMilli()), "",0.0,0.0,0.0);
    }

    public Inpatient(int patientid, Timestamp dateofstay,String roomnumber, double dailyrate, double supplies, double services) {
        super();
        this.PATIENTID = patientid;
        this.DATEOFSTAY = dateofstay;
        this.ROOMNUMBER = roomnumber;
        this.DAILYRATE = dailyrate;
        this.SUPPLIES = supplies;
        this.SERVICES = services;
    }
  
  

    public List<Surgical> getListOfSurgical() {
        return listOfSurgical;
    }

    public void setListOfSurgical(List<Surgical> listOfSurgical) {
        this.listOfSurgical = listOfSurgical;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getPATIENTID() {
        return PATIENTID;
    }

    public void setPATIENTID(int PATIENTID) {
        this.PATIENTID = PATIENTID;
    }

    public Timestamp getDateofstay() {
        return DATEOFSTAY;
    }

    public void setDateofstay(Timestamp dateofstay) {
        this.DATEOFSTAY = dateofstay;
    }

    public String getROOMNUMBER() {
        return ROOMNUMBER;
    }

    public void setROOMNUMBER(String ROOMNUMBER) {
        this.ROOMNUMBER = ROOMNUMBER;
    }

    public double getDAILYRATE() {
        return DAILYRATE;
    }

    public void setDAILYRATE(double DAILYRATE) {
        this.DAILYRATE = DAILYRATE;
    }

    public double getSUPPLIES() {
        return SUPPLIES;
    }

    public void setSUPPLIES(double SUPPLIES) {
        this.SUPPLIES = SUPPLIES;
    }

    public double getSERVICES() {
        return SERVICES;
    }

    public void setSERVICES(double SERVICES) {
        this.SERVICES = SERVICES;
    }

    @Override
    public String toString() {
        return "Inpatient{" + "ID=" + ID + ", PATIENTID=" + PATIENTID + ", dateofstay=" + DATEOFSTAY + ", ROOMNUMBER=" + ROOMNUMBER + ", DAILYRATE=" + DAILYRATE + ", SUPPLIES=" + SUPPLIES + ", SERVICES=" + SERVICES + '}';
    }
  
}
